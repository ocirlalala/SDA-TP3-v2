import java.util.Map;
import java.util.TreeMap;
import java.util.List;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.Stack;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;


/**
 * Kelas utama untuk pengerjaan Tugas Pemrograman 3
 * @author Rico Putra Pradana - 1606892163 - SDA D
 */
public class SDA1606892163TP3 {
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		Solution sol = new Solution(in);
		sol.main();
	}	
	
}

class Solution {
	private BufferedReader in;
	FileStorage fileStorage = new FileStorage();

	public Solution(BufferedReader in) {
		this.in = in;
	}
	
	public void main() throws IOException
	{
		String input = null;
		while ((input = in.readLine()) != null) {
			String[] command = input.split(" ");
			
			if (command[0].equals("add")) {
				String childFolName = command[1];
				String parentFolName = command[2];
				fileStorage.insertFolder(childFolName, parentFolName);
				updateSizeHelper();
			}
			// insert A.B C D
			else if (command[0].equals("insert")) {
				String[] childFile = command[1].split("\\.");
				
				String childName = childFile[0];
				String childType = childFile[1];
				
				int fileSize = Integer.parseInt(command[2]);
				String parentFolName = command[3];
				
				fileStorage.insertFile(childName, childType, fileSize, parentFolName);
				updateSizeHelper();
			}
			else if (command[0].equals("remove")) {
				String name = command[1];
				fileStorage.remove(name);
			}
			else if (command[0].equals("search")) {
				String name = command[1];
				fileStorage.search(name);
			}
			else if (command[0].equals("print")) {
				String name = command[1];
				fileStorage.printFolder(name);
			}
		}
	}
	
	private int updateSizeHelper() {
		return updateSize(fileStorage.getRoot());
	}
	
	private int updateSize(Folder folder)
	{
		if (folder.getListOfFolder().isEmpty() && folder.getListOfFile().isEmpty()) {
			folder.setSize(1);
			return 1;
		}
		else if (!folder.getListOfFolder().isEmpty()) {
			int count = 0;
			for (Folder folderChild : folder.getListOfFolder()) {
				count += updateSize(folderChild);
			}
			count++;
			folder.setSize(count);
			return count;
		}
		else if (!folder.getListOfFile().isEmpty()) {
			int count = 0;
			for (File fileChild : folder.getListOfFile()) {
				count += 1 + fileChild.getSize();
			}
			folder.setSize(count);
			return count;
		}
		return 0;
	}
}

/**
 * Kelas yang merepresentasikan sebuah File Storage
 * @author Rico Putra Pradana
 */
class FileStorage {
	private Folder root;
	private Map<String, Folder> mapOfAllFolder; // for "print <folder-name>" command
	private Map<String, ArrayList<Folder>> mapFolderOfThisFile; // for "search <file-name>" command || String : "<nameFile/nameFolder>-<fileType/folOfFol/folOfFile>"
	private Map<String, ArrayList<File>> mapOfAllFile; // for "remove "
	
	public FileStorage() {
		this.root = new Folder("root");
		this.mapOfAllFolder = new TreeMap<String, Folder>();
		mapOfAllFolder.put("root", root);
		
		this.mapFolderOfThisFile = new TreeMap<String, ArrayList<Folder>>();
		
		/**
		 * Kalau hapus file dengan nama tsb, semua file dengan nama tsb akan dihapus
		 */
		this.mapOfAllFile = new TreeMap<String, ArrayList<File>>();
	}
	
	public Folder getRoot() {
		return this.root;
	}
	
	/**
	 * Method untuk memasukkan sebuah file pada sebuah folder
	 * @param fileName : nama file yang akan dimasukkan
	 * @param type : tipe file yang akan dimasukkan
	 * @param size : ukuran file yang akan dimasukkan
	 * @param destinationFolderName : nama folder tujuan
	 */
	public void insertFile(String fileName, String type, int size, String destinationFolderName)
	{
		if (containsFolder(destinationFolderName)) {
			insertFile(fileName, size, type, destinationFolderName);
		} 
	}
	
	/**
	 * Method untuk memasukkan file ke dalam folder tujuan
	 * @param newFileName - Nama file yang akan dimasukkan
	 * @param size - Ukuran file yang akan dimasukka
	 * @param fileType - Tipe file yang akan dimasukkan
	 * @param destinationFolderName - Nama folder tujuan
	 * @return Folder yang berhasil menampung file yang dimasukkan
	 */
	private Folder insertFile(String newFileName, int size, String fileType, String destinationFolderName)
	{
		int destinationFolderIndexInParent = mapOfAllFolder.get(destinationFolderName).getMyIndexInParentFolder();
		
		Folder folderHost = mapOfAllFolder.get(destinationFolderName).insertFile(newFileName, size, fileType, destinationFolderIndexInParent, destinationFolderIndexInParent);
		
		if (folderHost != null) {
			File newFile = new File(folderHost, newFileName, size, fileType);
			
			if (!containsFile(newFileName)) {
				mapOfAllFile.put(newFileName, new ArrayList<File>());
				mapFolderOfThisFile.put(newFileName, new ArrayList<Folder>());
			}
			mapFolderOfThisFile.get(newFileName).add(folderHost);
			mapOfAllFile.get(newFileName).add(newFile);
//			mapOfAllFolder.get(destinationFolderName).setSize(mapOfAllFolder.get(destinationFolderName).getSize() + size);
			return folderHost;
		}
		return null;	
	}
	
	/**
	 * Method untuk memasukkan sebuah folder ke dalam folder lainnya 
	 * @param newFolderName : nama folder yang akan dimasukkan
	 * @param destinationFolderName : nama folder tujuan
	 */
	public void insertFolder(String newFolderName, String destinationFolderName) 
	{
		if ((!containsFolder(newFolderName)) && (containsFolder(destinationFolderName))) {
			Folder newFolder = new Folder(root, newFolderName);
			Folder destinationFolder = insertFolder(newFolder, newFolderName, destinationFolderName);
			newFolder.setParent(destinationFolder);
		}
	}
	
	/**
	 * Method untuk menambahkan sebuah folder ke dalam folder tujuan
	 * @param newFolder - Folder baru yang akan dimasukkan
	 * @param newFolderName - Nama folder baru yang akan dimasukkan
	 * @param destinationFolderName - Nama folder tujuan
	 * @return Folder tujuan yang akan menampung newFolder
	 */
	private Folder insertFolder(Folder newFolder, String newFolderName, String destinationFolderName) 
	{
		Folder destinationFolder = mapOfAllFolder.get(destinationFolderName);
		
//		if (destinationFolder.getSize() > 1) {
			if (destinationFolder.getType().equals("folderOfFile")) {
				moveFilesToNewFolder(newFolder, destinationFolder);
				updateMapFolderOfThisFile(newFolder, destinationFolder);
				destinationFolder.setType("folderOfFolder");
			}
//		}
//		else {
//			destinationFolder.setType("folderOfFolder");
//		}
		mapOfAllFolder.put(destinationFolderName, destinationFolder);
		mapOfAllFolder.put(newFolderName, newFolder);
		destinationFolder.getListOfFolder().add(newFolder);
		return destinationFolder;
	}
	
	/**
	 * Method untuk memindahkan seluruh file yang dipindahkan dari destinationFolder ke newFolder
	 * @param newFolder - Folder baru yang akan menyimpan file-file yang dipindahkan
	 * @param destinationFolder - Folder lama yang akan memindahkan file-filenya ke newFolder
	 */
	public void moveFilesToNewFolder(Folder newFolder, Folder destinationFolder)
	{
		newFolder.setListOfFile(destinationFolder.getListOfFile());
		destinationFolder.getListOfFile().clear();
		newFolder.setFileInThisFolder(destinationFolder.getFileInThisFolder());
		destinationFolder.getFileInThisFolder().clear();
	}
	
	/**
	 * Method untuk mengganti folder-folder untuk setiap fileYangDipindahkan di
	 * "mapFolderOfThisFile" dari destinationFolder menjadi newFolder
	 * @param newFolder - Folder baru yang akan menyimpan file-file yang dipindahkan
	 */
	public void updateMapFolderOfThisFile(Folder newFolder, Folder destinationFolder)
	{
		for (File fileYangDipindahkan : newFolder.getListOfFile()) {
			mapFolderOfThisFile.get(fileYangDipindahkan.getName()).remove(destinationFolder);
			mapFolderOfThisFile.get(fileYangDipindahkan.getName()).add(newFolder);
		}
	}
	
	/**
	 * Method untuk mengecek keberadaan sebuah file
	 * @param fileName : Nama file yang dicari
	 * @return true jika file ditemukan, false jika file tidak ditemukan
	 */
	public boolean containsFile(String fileName)
	{
		return mapFolderOfThisFile.containsKey(fileName);
	}
	
	/**
	 * Method untuk mengecek keberadaan sebuah folder
	 * @param destinationFolderName : Nama folder yang dicari
	 * @return true jika folder ditemukan, false jika tidak
	 */
	public boolean containsFolder(String destinationFolderName)
	{
		return mapOfAllFolder.containsKey(destinationFolderName);
	}
	
	/**
	 * Method untuk mencetak isi dari suatu folder
	 * @param folderName - Nama folder yang akan dicetak beserta isinya
	 */
	public void printFolder(String folderName)
	{
		if (containsFolder(folderName)) {
			Folder folder = mapOfAllFolder.get(folderName);
			String outFirst = "> " + folderName + " " + folder.getSize();
			
			ArrayList<String> out = new ArrayList<String>();
			out.add(folderName + " " + folder.getSize() + "-0");
			printFol(folder, out, 0);
//			System.out.println(outFirst);
			printReal(out);
		}
		
	}
		
	/**
	 * Method untuk mencetak isi dari suatu folder
	 * @param folder - Folder yang akan dicetak isinya
	 * @param out - Sebuah arrayList<String> yang menyimpan folder/file yang akan dicetak beserta depth nya
	 * @param depth - kedalaman folder/file yang akan menjadi spasi
	 * @return ArrayList<String> yang berisi folder/file yang akan dicetak 
	 */
	private void printFol(Folder folder, ArrayList<String> out, int depth)
	{
		Collections.sort(folder.getListOfFolder());
		depth += 2;
		if (folder.getListOfFolder().isEmpty() && folder.getListOfFile().isEmpty()) {
			return;
		}
		else if (!folder.getListOfFolder().isEmpty()) {
			for (Folder folderChild : folder.getListOfFolder()) {
				out.add(folderChild.getName() + " " + folderChild.getSize() + "-" + depth);
				printFol(folderChild, out, depth);
			}
		}
		else if (!folder.getListOfFile().isEmpty()) {
			for (File fileChild : folder.getListOfFile()) {
				out.add(fileChild.getName() + "." + fileChild.getType() + " " + fileChild.getSize() + "-" + depth);
				return;
			}
		}
	}

	/**
	 * Method untuk mencetak keluaran akhir dari struktur folder yang akan dicetak
	 * @param outList - List yang berisi folder/file yang akan dicetak dengan format string tertentu
	 */
	private void printReal(ArrayList<String> outList) 
	{
		//System.out.println(outList);
		String out = "";
		for (String str : outList) {
			String[] strBreak = str.split("-");
			String name = strBreak[0];
			int depth = Integer.parseInt(strBreak[1]);
			String space = "";
			
			for (int i = 0; i < depth; i++) {
				space += " ";
			}
			
			out += space + "> " + name + "\n"; 	
		}
		System.out.println(out);
	}
	
	/**
	 * Method utama untuk menghapus file atau folder beserta seluruh isinya
	 * @param name - nama file/folder yang akan dihapus
	 */
	public void remove(String name)
	{
		if (containsFile(name)) {
			int jmlHapus = 0;
			for (File file : mapOfAllFile.get(name)) {
				// hapus setiap file yang memiliki nama yang sama
				jmlHapus += removeFile(file.getName());
			}
			System.out.println(jmlHapus + " File " + name + " removed");
		}
		else if (containsFolder(name)) {
			// hapus folder beserta seluruh isinya
			removeFolder(name);
			System.out.println("Folder " + name + " removed");
		}
	}
	
	/**
	 * Method untuk menghapus sebuah folder beserta isinya
	 * @param name - Nama folder yang akan dihapus
	 * @return Folder yang dihapus
	 */
	public Folder removeFolder(String name)
	{
		Folder folderToBeDeleted = mapOfAllFolder.get(name);
		
		// jika folder kosong -> hapus folder ini
		if (folderToBeDeleted.getListOfFolder().isEmpty() && folderToBeDeleted.getListOfFile().isEmpty()) {
	
			//hapus folder ini dari listOfFolder parent nya
			mapOfAllFolder.get(name).getParent().getListOfFolder().remove(folderToBeDeleted);
		
			// hapus folder ini dari mapOfAllFolder
			mapOfAllFolder.remove(name);
			
			// kembalikan folder ini
			return folderToBeDeleted;
		}
		// jika berisi folderOfFolder -> telurusi hingga ke folder terdalam
		else if (!folderToBeDeleted.getListOfFolder().isEmpty()) {
			// telurusi sampai ke folder terdalam -> DFS
			for (Folder folderChild : folderToBeDeleted.getListOfFolder()) {
				return removeFolder(folderChild.getName());
			}
		}
		// jika berupa folderOfFile -> hapus setiap file
		else if (!folderToBeDeleted.getListOfFile().isEmpty()){
			for (File fileToBeDeleted : folderToBeDeleted.getListOfFile()) {
				removeFile(fileToBeDeleted.getName());
//				return null
			}
			return folderToBeDeleted;
		}
		return null;
	}
	
	/**
	 * Menghapus semua file dengan nama yang sama
	 * @param name - Nama file yang akan dihapus
	 * @return 
	 */
	public int removeFile(String name)
	{
		int jmlHapus = mapOfAllFile.get(name).size();
		for (File fileToBeDeleted : mapOfAllFile.get(name)) {
			// hapus keberadaan file ini dari parent folder nya
			fileToBeDeleted.getParent().getListOfFile().remove(fileToBeDeleted);
			fileToBeDeleted.getParent().getFileInThisFolder().remove(name);
		}
		
		// hapus keberadaannya secara global dari mapOfAllFile
		mapOfAllFile.remove(name);
		
		return jmlHapus;
	}
	
	/**
	 * Method untuk mencari keberadaan sebuah file/folder dengan nama A di dalam File Storage
	 * dan mencetak path dari root hingga file/folder tersebut
	 * @param name - Nama file/folder yang dicari
	 */
	public void search(String name)
	{
		if (containsFolder(name))
		{
			Folder folderToBeSearched = mapOfAllFolder.get(name);
			Stack<Folder> path = new Stack<Folder>();
			path = searchFolder(folderToBeSearched, path);
			printFoundFolder(path);
		}
		else if (containsFile(name))
		{
//			ArrayList<File> lstFileToBeSearched = mapOfAllFile.get(name);
			ArrayList<String> out = new ArrayList<String>();
			searchFile(root, name, out, 0);
			printReal(out);
		}
	}
	
	/**
	 * Method untuk mencetak path folder yang ditemukan
	 * @param path - Riwayat keberadaan folder
	 */
	private void printFoundFolder(Stack<Folder> path)
	{
		int depth = 0;
		String out = "";
		
		for (int i = 0; i < path.size(); i++) {
			Folder folder = path.pop();
			out += depth + "> " + folder + " " + folder.getSize();
			depth += 2;
		}
	}
	
	/**
	 * Method untuk mencari keberadaan sebuah folder dan mencatat path nya di dalam stack
	 * @param folderToBeSearched - Folder yang dicari keberadaannya di dalal File Storage
	 * @param path - Stack yang menyimpan jalur keberadaan folder yang dicari
	 * @return Jalur keberadaan folder yang dicari dari root hingga folder tersebut
	 */
	private Stack<Folder> searchFolder(Folder folderToBeSearched, Stack<Folder> path)
	{
		// base case
		if (folderToBeSearched.getParent() == null) {
			path.push(folderToBeSearched);
			return path;
		}
		else {
			path.push(folderToBeSearched);
			return searchFolder(folderToBeSearched.getParent(), path);
		}
	}

	/**
	 * Method untuk mengecek keberadaan satu/lebih file sekaligus mencatat path nya dari root
	 * @param folder : Folder yang akan dicek apakah memiliki file yang dicari - di awali dengan root
	 * @param fileName : Nama file yang dicari
	 * @param out : ArrayList<String> yang menyimpan path
	 * @param depth : Kedalaman file/folder yang ditelusuri
	 * @return boolean keberadaan sebuah file
	 */
	private boolean searchFile(Folder folder, String fileName, ArrayList<String> out, int depth) 
	{
		boolean isAda = false;
//		out.add(folder.getName() + " " + folder.getSize() + "-" + depth);
		out.add(folder.getName() + "-" + depth);
		
		// base case : jika sudah sampai folder yang berisi file
		if (!folder.getListOfFile().isEmpty()) {
			int depthFile = depth + 2;
			for (File fileChild : folder.getListOfFile()) {
				if (fileChild.getName().equals(fileName)) {
//					out.add(fileChild.getName() + " ." + fileChild.getType() + " "  + fileChild.getSize() + "-" + depthFile);
					out.add(fileChild.getName() + "." + fileChild.getType() + "-" + depthFile);
					isAda = true;
				}
			}
		}
		else if (folder.getListOfFile().isEmpty() && folder.getListOfFolder().isEmpty()) {
			isAda = false;
		}
		else if (!folder.getListOfFolder().isEmpty()) {
			for (Folder folderChild : folder.getListOfFolder()) {
//				isAda = searchFile(folderChild, fileName, out, depth + 2);
				isAda |= searchFile(folderChild, fileName, out, depth + 2);
			}
		}
		if (isAda == false) {
			out.remove(out.size()-1);
		}
		
		return isAda;
	}

}

/**
 * Kelas yang merepresentasikan sebuah File
 * @author Rico Putra Pradana
 */
class File implements Comparable<File> {
	private Folder parent;
	private String name;
	private int size;
	private String type; // ex: exe, txt, docx, pptx
	
	public File(Folder parent, String name, int size, String type) {
		this.parent = parent;
		this.name = name;
		this.size = size;
		this.type = type;
	}
	
	/**
	 * Method untuk membandingkan 2 buah file
	 */
	@Override
	public int compareTo(File otherFile)
	{
		if (this.name.equals(otherFile.getName())) {
			if (this.type.equals(otherFile.getType())) {
				return this.size - otherFile.getSize();
			}
			return this.type.compareTo(otherFile.getType());
		}
		return this.name.compareTo(otherFile.getName());
	}

	public Folder getParent() {
		return parent;
	}

	public void setParent(Folder parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

/**
 * Kelas yang merepresentasikan sebuah Folder
 * @author Rico Putra Pradana
 */
class Folder implements Comparable<Folder> {
	private Folder parent;
	private String name;
	private int size;
	private String type; // folderOfFile or folderOfFolder -> ditentukan ketika dimasukkan sebuah file atau folder
	private String fileType;
	private Map<String, ArrayList<File>> fileInThisFolder;
//	private Map<String, Folder> folderInThisFolder; // GAK GUNA SEPERTINYA -> BUANG AJA NANTI
	private List<File> listOfFile; // for sorting
	private List<Folder> listOfFolder; // for sorting
	private int myIndexInParentFolder;
	
	public Folder(String name) {
		this.parent = null;
		this.name = name;
		this.size = 1;
		this.type = "none";
		this.fileType = "none";
		this.fileInThisFolder = new TreeMap<String, ArrayList<File>>();
		this.listOfFile = new ArrayList<File>();
		this.listOfFolder = new ArrayList<Folder>();
		this.myIndexInParentFolder = 0;
	}
	
	public Folder(Folder parent, String name) {
		this.parent = parent;
		this.name = name;
		this.size = 1;
		this.type = "none";
		this.fileType = "none";
		this.fileInThisFolder = new TreeMap<String, ArrayList<File>>();
		this.listOfFile = new ArrayList<File>();
		this.listOfFolder = new ArrayList<Folder>();
		this.myIndexInParentFolder = parent.getListOfFolder().indexOf(this);
	}
	
	
	
	/**
	 * Method untuk memasukkan sebuah file pada folder ini
	 * @param fileName - Nama file yang akan dimasukkan
	 * @param size - Ukuran file yang akan dimasukkan
	 * @param fileType - Tipe/extension file yang akan dimasukkan
	 * @param myFolderIndexInParent - Index folder ini dalam parent.listOfFolder
	 * @param flagIndex - Index yang menandai pada index berapakah dalam parent.listOfFolder ketika memasukkan sebuah file ke dalam folder ini
	 * @return Folder terakhir yang bisa menerima file tersebut sebagai masukan
	 * 
	 * Selalu lakukan sorting pada ListOfFolder sebelum memanggil insertFile di FileStorage
	 */
	public Folder insertFile(String fileName, int size, String fileType, int myFolderIndexInParent, int flagIndex)
	{	
		// jika folder ini masih benar-benar kosong - CLEAR
		if (listOfFile.isEmpty() && listOfFolder.isEmpty()) {
			File fileBaru = new File(this, fileName, size, fileType);
			
			if (fileInThisFolder.isEmpty()) {
				fileInThisFolder.put(fileName, new ArrayList<File>());
			}
			else {
				fileInThisFolder.get(fileName).add(fileBaru);
			}
			
			listOfFile.add(fileBaru);
			System.out.println(fileName + "." + fileType + " added to " + this.name);
//			this.size++;
//			updateSizeHelper(root);
			this.type = "folderOfFile";
			this.fileType = fileType;
			return this;
		}
		// jika berupa folderOfFolder
		else if (!listOfFolder.isEmpty()) {
			if (flagIndex == 0) {
				Folder folder = listOfFolder.get(0).insertFile(fileName, size, fileType, 0, 0);
				if (folder != null) {
					return folder; // folder yang bisa menampung file yg diinsert
				}
				
				if (myFolderIndexInParent < parent.getListOfFolder().size() - 1) {
					myFolderIndexInParent++;
				}
				return parent.getListOfFolder().get(myFolderIndexInParent).insertFile(fileName, size, fileType, myFolderIndexInParent, flagIndex);
			} 
			else {
				if (myFolderIndexInParent == flagIndex - 1) {
					return listOfFolder.get(flagIndex - 1).insertFile(fileName, size, fileType, 0, 0);
				}
				else {
					Folder folder = listOfFolder.get(0).insertFile(fileName, size, fileType, 0, 0);
					
					if (folder != null) {
						return folder;
					}
					
					// kalau bukan di posisi terakhir -> jalankan dulu lalu lanjutkan
					if (myFolderIndexInParent < parent.getListOfFolder().size() - 1) {
						myFolderIndexInParent++;
					}
					// kalau sudah di posisi terakhir -> putar
					else if (myFolderIndexInParent == parent.getListOfFolder().size() - 1) {
						myFolderIndexInParent = 0;
					}
					return parent.getListOfFolder().get(myFolderIndexInParent).insertFile(fileName, size, fileType, myFolderIndexInParent, myFolderIndexInParent);
				}
			}
		}
		// jika berupa folderOfFile - CLEAR
		else if (!listOfFile.isEmpty()) 
		{
			if (this.fileType.equals(fileType)) {
				File fileBaru = new File(this, fileName, size, fileType);
				
				if (fileInThisFolder.isEmpty()) {
					fileInThisFolder.put(fileName, new ArrayList<File>());
				}
				else {
					fileInThisFolder.get(fileName).add(fileBaru);
				}
				
				listOfFile.add(fileBaru);
				System.out.println(fileName + "." + fileType + " added to " + this.name);
				this.size++;
				this.type = "folderOfFile";
				this.fileType = fileType;
				return this;
			}
			else {
				if (flagIndex == 0) {
					if (myFolderIndexInParent < parent.getListOfFolder().size() - 1) {
						myFolderIndexInParent++;
					}
					return parent.getListOfFolder().get(myFolderIndexInParent).insertFile(fileName, size, fileType, myFolderIndexInParent, flagIndex);
				}
				else {
					// kalau sudah di posisi sebelum posisi folder pertama, insertFile aja, ga usah geser folder lagi alias SELESAI
					if (myFolderIndexInParent == flagIndex - 1) {
						return parent.getListOfFolder().get(myFolderIndexInParent).insertFile(fileName, size, fileType, myFolderIndexInParent, flagIndex);
					}
					else {
						// kalau bukan di posisi terakhir -> lanjutkan
						if (myFolderIndexInParent < parent.getListOfFolder().size() - 1) {
							myFolderIndexInParent++;
						}
						// kalau sudah di posisi terakhir -> putar
						else if (myFolderIndexInParent == parent.getListOfFolder().size() - 1) {
							myFolderIndexInParent = 0;
						}
						return parent.getListOfFolder().get(myFolderIndexInParent).insertFile(fileName, size, fileType, myFolderIndexInParent, flagIndex);
					}
				}
			}
		}
		// Jika tidak terdapat folder kosong atau folder yang berisikan file dengan fileType yang sama
		return null;
	}
	
	@Override
	public int compareTo(Folder otherFol)
	{
		return this.name.compareTo(otherFol.getName());
	}

	public List<File> getListOfFile() {
		return listOfFile;
	}

	public void setListOfFile(List<File> listOfFile) {
		this.listOfFile = listOfFile;
	}

	public List<Folder> getListOfFolder() {
		return listOfFolder;
	}

	public void setListOfFolder(List<Folder> listOfFolder) {
		this.listOfFolder = listOfFolder;
	}

	public int getMyIndexInParentFolder() {
		return myIndexInParentFolder;
	}

	public void setMyIndexInParentFolder(int parentIndex) {
		this.myIndexInParentFolder = parentIndex;
	}

	public Folder getParent() {
		return parent;
	}

	public void setParent(Folder parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, ArrayList<File>> getFileInThisFolder() {
		return fileInThisFolder;
	}

	public void setFileInThisFolder(Map<String, ArrayList<File>> fileInThisFolder) {
		this.fileInThisFolder = fileInThisFolder;
	}

//	public Map<String, ArrayList<Folder>> getFolderInThisFolder() {
//		return folderInThisFolder;
//	}
//
//	public void setFolderInThisFolder(Map<String, ArrayList<Folder>> folderInThisFolder) {
//		this.folderInThisFolder = folderInThisFolder;
//	}
}