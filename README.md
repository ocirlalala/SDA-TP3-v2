# Tugas Pemrograman 3 : Magical File Storage

CSGE602040 - Struktur Data & Algoritma (Data Structures & Algorithms) @ Fakultas Ilmu
Komputer Universitas Indonesia, Semester Gasal 2017/2018


***

## Ketentuan Program

- Nama berkas kode sumber		: SDANPMT3.java
- Batas waktu eksekusi program: 3 detik / kasus uji
- Batas memori program		: 256 MB / kasus uji
- Batas waktu pengumpulan		: Minggu, 10 Desember 2017 pukul 23.55

## File Storage

- Terdiri dari 2 komponen, yaitu Folder dan File
- Suatu Folder A dapat berisi banyak Folder lainnya namun tidak terdapat File di dalam Folder A
- Jika sebuah File A dimasukkan ke dalam Folder B, namun Folder B berisikan Folder lain, maka File A akan dimasukkan ke dalam Folder yang berada di dalam Folder B. Hal ini dilakukan terus menerus hingga tidak ada Folder lagi di dalam sebuah Folder.
- Urutan berdasarkan **leksikografis** NAMA FOLDER

## FILE

- Memiliki nama dan tipe
- Memiliki ukuran tersendiri

## FOLDER

- Ukuran Folder adalah `1 + ukuran seluruh File dan/atau Folder yang ada di dalamnya`
- Hanya berisikan **SATU TIPE File** saja
- Jika sebuah **File bertipe M** dimasukkan ke dalam sebuah Folder J yang sudah berisikan **File bertipe N**, maka File bertipe M akan dimasukkan ke dalam Folder **selanjutnya** dari urutan **leksikografis** berdasarkan nama Folder tersebut. Hal ini dilakukan terus-menerus hingga menemukan folder kosong atau berisikan File bertipe sama.
- Jika Folder selanjutnya berisi Folder lainnya, maka File dimasukkan ke dalam Folder yang ada di dalamnya.
- Jka tidak ada Folder selanjutnya, alias Folder terakhir dalam urutan leksikografis, maka Folder yang menjadi Folder selanjutnya adalah **Folder pertama** dalam urutan leksikografis alias **memutar**
- Jika di seluruh File Storage tidak ada Folder kosong maupun File yang bertipe sama, maka File tidak jadi dimasukkan.

## Basic Operation

1. **add A B**
	- Memasukkan **Folder A** ke dalam **Folder B**
	- ex: `add folderA root`
	- Perintah ini tidak memiliki keluaran
2. **insert A.B C D**
	- Memasukkan **File bernama A bertipe B** dengan **ukuran C** ke dalam Folder D
	- ex: `insert santoryuu.exe 5 root`
	- Keluaran : "santoryuu.exe added to X"
	- X adalah **nama Folder** tempat A **akhirnya** dimasukkan
3. **remove A**
	- Menghapus File/Folder bernama A
	- Jika A adalah Folder, maka hapus **seluruh isinya** juga
	- Jika A adalah File, maka hapus **semua File** yang memiliki **nama yang sama**
	- ex: `remove santoryuu`
	- Keluaran : 
		- Jika santoryuu adalah Folder : "Folder santoryuu removed"
		- Jika santoryuu adalah File : "X File santoryuu removed"
		- X adalah **jumlah** File santoryuu yang berhasil dihapus
4. **search A**
	- Mencari semua Fle/Folder dengan nama A di dalam File Storage nya
	- ex: `search KongGun`
	- Keluaran:
		``` 
		> root
			> folderA
				> KongGun.dock
			> folderB
				> KongGun.xlsx
		```
	- Cetak **semua path** dari & menuju File/Folder yang dicari
	- Jika yang dicari adalah File, cetak juga **tipe filenya**
5. **print A**
	- Mencetak isi dari Folder bernama A
	- Setiap Folder/File yang berada **1 level lebih dalam** dicetak dengan indentasi **2 spasi lebih kanan** dari Folder di atasnya
	- ex : `print root`
	- Keluaran :
		``` 
		> root 9
			> folderA 8
				> nihilego.exe 3
				> santoryuu.exe 4
		```

## Riwayat Pengerjaan

1.	**add A B**
	- Solution.main() ke blok berikut :
 		```
 		if (command[0].equals("add")) {
				String childFolName = command[1];
				String parentFolName = command[2];
				fileStorage.insertFolder(childFolName, parentFolName);
				updateSizeHelper();
		}
		```

		fileStorage : object FileStorage()
	- FileStorage : public void insertFolder(String newFolderName, String destinationFolderName)
	- FileStorage : private Folder insertFolder(Folder newFolder, String newFolderName, String destinationFolder)
	 	- Jika destinationFolder sebelumnya sudah berisi file-file:
	 		a. FileStorage : public void moveFilesToNewFolder(Folder newFolder, Folder destinationFolder)
		 	b. FileStorage : public void updateMapFolderOfThisFIle(Folder newFOlder, Folder destinationFolder)
	- Solution : private int updateSizeHelper()
	- Solution : private int updateSize(Folder folder)

2. **insert A.B C D**
	- Solution.main() ke blok berikut :
 		```
 		else if (command[0].equals("insert")) {
			String[] childFile = command[1].split("\\.");
			String childName = childFile[0];
			String childType = childFile[1];
			int fileSize = Integer.parseInt(command[2]);
			String parentFolName = command[3];
			fileStorage.insertFile(childName, childType, fileSize, parentFolName);
			updateSizeHelper();
		}
		```
	- FileStorage : public void insertFile(String fileName, String type, int size, String destinationFolderName)
	- FileStorage : private Folder insertFile(String newFileName, int size, String fileType, String destinationFolderName)
	- Folder : public Folder insertFile(String filename, int size, String fileType, int myFolderIndexInParent, int flagIndex)
	- Solution : private int updateSizeHelper()
	- Solution : private int updateSize(Folder folder)

3. **remove A**
	- Solution.main() ke blok berikut:
		```
		else if (command[0].equals("remove")) {
			String name = command[1];
			fileStorage.remove(name);
		}
		```
	- FileStorage : public void remove(String name)
		a. Jika folder ada dan berisi File :
			- FileStorage : public int removeFile(String name)
		b. Jika folder ada dan berisi Folder :
			- FileStorage : public Folder removeFolder(String name)

4. **search A**
	- Solution.main() ke blok berikut:
		```
		else if (command[0].equals("search")) {
			String name = command[1];
			fileStorage.search(name);
		}
		```
	- FileStorage : public void search(String name)
		a. Jika yang dicari adalah folder:
			- FileStorage : private Stack<Folder> searchFolder(Folder folderToBeSearched, Stack<Folder> path)
			- FileStorage : private void printFoundFolder(Stack<Folder> path)
		b. Jika yang dicari adalah file:
			- FileStorage : private boolean searchFile(Folder folder, String fileame, ArrayList<String> out, int depth)
			- FileStorage : private void printReal(ArrayList<String> outList)

5. **print A**
	- Solution.main() ke blok berikut:
		```
		else if (command[0].equals("print")) {
			String name = command[1];
			fileStorage.printFolder(name);
		}
		```
	- FileStorage : public void printFolder(String folderName)
	- FileStorage : private void printFol(Folder folder, ArrayList<String> out, int depth)
	- FileStorage : private void printReal(ArrayList<String> outList)